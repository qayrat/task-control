package com.test.room.taskslist

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.test.room.taskslist.data.Task
import com.test.room.taskslist.database.TaskDao
import com.test.room.taskslist.database.TaskRoomDatabase
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class TaskRoomDatabaseTest {

    private lateinit var taskDao: TaskDao
    private lateinit var db: TaskRoomDatabase

    @Before
    fun createDB() {
        val context: Context = ApplicationProvider.getApplicationContext()
        db = Room.inMemoryDatabaseBuilder(context, TaskRoomDatabase::class.java)
            .allowMainThreadQueries()
            .build()
        taskDao = db.taskDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDB() {
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun insertAndGetTask() = runBlocking {
        val task = Task(0, "task1", "5 April 2022", "12:00", "progress")
        taskDao.insert(task)
        val allTasks = taskDao.getAllTasks().first()
        assertEquals(allTasks[0].name, task.name)
    }

    @Test
    @Throws(Exception::class)
    fun getAllTasks() = runBlocking {
        val task = Task(0, "task2", "5 April 2022", "12:00", "progress")
        taskDao.insert(task)
        val task2 = Task(0, "task3", "5 April 2022", "12:00", "progress")
        taskDao.insert(task2)
        val allTasks = taskDao.getAllTasks().first()
        assertEquals(allTasks[0].name, task.name)
        assertEquals(allTasks[1].name, task2.name)
    }

    @Test
    @Throws(Exception::class)
    fun delete() = runBlocking {
        val task = Task(0, "task4", "5 April 2022", "12:00", "progress")
        taskDao.insert(task)
        var allTasks = taskDao.getAllTasks().first()
        taskDao.delete(allTasks[0])
        allTasks = taskDao.getAllTasks().first()
        assertTrue(allTasks.isEmpty())
    }

    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("com.test.room.taskslist", appContext.packageName)
    }
}