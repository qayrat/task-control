package com.test.room.taskslist.utils

import android.view.View
import androidx.fragment.app.Fragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.test.room.taskslist.R
import com.test.room.taskslist.data.Task
import com.test.room.taskslist.ui.TaskViewModel
import kotlinx.android.synthetic.main.fragment_all_tasks.*

fun Fragment.showDeleteTaskDialog(
    desc: String,
    fragment: Fragment,
    viewModel: TaskViewModel,
    task: Task
) {
    MaterialAlertDialogBuilder(fragment.requireContext())
        .setTitle(resources.getString(R.string.delete))
        .setMessage(desc)
        .setPositiveButton(resources.getString(R.string.yes)) { dialog, which ->
            viewModel.delete(task)
            fragment.showSnackbar(fragment.layout, getString(R.string.task_delete))
        }
        .setNegativeButton(resources.getString(R.string.no)) { dialog, which ->
            dialog.dismiss()
        }
        .show()
}

fun Fragment.showSnackbar(view: View, message: String) {
    Snackbar.make(view, message, Snackbar.LENGTH_SHORT).show()
}