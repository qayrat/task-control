package com.test.room.taskslist

import android.app.Application
import com.test.room.taskslist.database.TaskRoomDatabase
import com.test.room.taskslist.ui.TaskRepository

class TaskApplication : Application() {
    private val database by lazy { TaskRoomDatabase.getDatabase(this) }
    val repository by lazy { TaskRepository(database.taskDao()) }
}