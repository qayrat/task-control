package com.test.room.taskslist.ui

import androidx.annotation.WorkerThread
import com.test.room.taskslist.data.Task
import com.test.room.taskslist.database.TaskDao
import kotlinx.coroutines.flow.Flow

class TaskRepository(private val taskDao: TaskDao) {
    val allTasks: Flow<List<Task>> = taskDao.getAllTasks()

    @WorkerThread
    suspend fun getAllTasksStatus(status: String) : Flow<List<Task>> {
        return taskDao.getAllTasks(status)
    }

    @WorkerThread
    suspend fun insert(task: Task) {
        taskDao.insert(task)
    }

    @WorkerThread
    suspend fun delete(task: Task) {
        taskDao.delete(task)
    }

    @WorkerThread
    suspend fun update(task: Task) {
        taskDao.update(task)
    }
}