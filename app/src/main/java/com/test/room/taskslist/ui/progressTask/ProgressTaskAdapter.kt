package com.test.room.taskslist.ui.progressTask

import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import androidx.annotation.MenuRes
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.test.room.taskslist.R
import com.test.room.taskslist.data.Task
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.fragment_all_task_item.*

class ProgressTaskAdapter(
    private val fragment: Fragment
) : RecyclerView.Adapter<ProgressTaskAdapter.ViewHolder>() {
    private val layoutInflater = LayoutInflater.from(fragment.requireContext())
    private val listener = fragment as OnItemChangeListener
    private var data: List<Task> = ArrayList()

    fun updateList(tasks: List<Task>) {
        data = tasks
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = layoutInflater.inflate(R.layout.fragment_all_task_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.binData(data[position])

    override fun getItemCount(): Int = data.size

    inner class ViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView), LayoutContainer {
        override val containerView: View
            get() = itemView
        private lateinit var itemData: Task

        init {
            menu.setOnClickListener {
                showMenu(menu, R.menu.menu_task_item)
            }
        }

        fun binData(task: Task) {
            itemData = task
            title.text = task.name
            date.text = task.date
            time.text = task.time
            if (task.status == "progress") {
                checkbox.isChecked = false
                checkbox.isEnabled = true
            } else {
                checkbox.isChecked = true
                checkbox.isEnabled = false
            }
            checkbox.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    Log.d("ProgressTaskAdapter", "task done!!!")
                    listener.taskDone(Task(task.id, task.name, task.date, task.time, "done"))
                    checkbox.isEnabled = false
                }
            }
        }

        private fun showMenu(view: View, @MenuRes menuRes: Int) {
            val popup = PopupMenu(fragment.requireContext(), view)
            popup.menuInflater.inflate(menuRes, popup.menu)
            popup.setOnMenuItemClickListener { menuItem: MenuItem ->
                when (menuItem.itemId) {
                    R.id.action_update -> {
                        listener.update(itemData)
                    }
                    R.id.action_delete -> {
                        listener.delete(itemData)
                    }
                }
                false
            }
            popup.show()
        }
    }

    interface OnItemChangeListener {
        fun taskDone(task: Task)
        fun update(task: Task)
        fun delete(task: Task)
    }
}