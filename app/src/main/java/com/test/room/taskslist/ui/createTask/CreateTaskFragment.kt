package com.test.room.taskslist.ui.createTask

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.activityViewModels
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.timepicker.MaterialTimePicker
import com.google.android.material.timepicker.TimeFormat
import com.test.room.taskslist.R
import com.test.room.taskslist.TaskApplication
import com.test.room.taskslist.data.Task
import com.test.room.taskslist.ui.TaskViewModel
import com.test.room.taskslist.ui.TaskViewModelFactory
import com.test.room.taskslist.utils.showSnackbar
import kotlinx.android.synthetic.main.fragment_create_task.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class CreateTaskFragment : BottomSheetDialogFragment() {
    private val viewModel: TaskViewModel by activityViewModels {
        TaskViewModelFactory((requireActivity().application as TaskApplication).repository)
    }
    private lateinit var calendar: Calendar
    private lateinit var df: DateFormat
    private var taskId = -1
    private var taskName = ""
    private var taskDate = ""
    private var taskTime = ""
    private var taskStatus = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            taskId = it.getInt("taskId")
            taskName = it.getString("taskName").toString()
            taskDate = it.getString("taskDate").toString()
            taskTime = it.getString("taskTime").toString()
            taskStatus = it.getString("taskStatus").toString()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_create_task, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"))
        df = SimpleDateFormat("d MMM yyyy")

        if (taskId != -1) {
            et_title.setText(taskName)
            et_date.setText(taskDate)
            et_time.setText(taskTime)
            tv_title.setText(R.string.update_task)
            create_task.setText(R.string.update_task)
        } else {
            tv_title.setText(R.string.title_create_task)
            create_task.setText(R.string.title_create_task)
        }

        create_task.setOnClickListener {
            if (et_time.text.toString().isNotEmpty() && et_date.text.toString().isNotEmpty() &&
                et_title.text.toString().isNotEmpty()
            ) {
                changeTask()
                dismiss()
            } else {
                showSnackbar(design_bottom_sheet, getString(R.string.fill_all_fields))
            }
        }

        et_date.setOnClickListener {
            openDatePicker()
        }

        et_time.setOnClickListener {
            openTimePicker()
        }
    }

    private fun changeTask() {
        if (taskId != -1) {
            viewModel.update(
                Task(
                    id = taskId,
                    name = et_title.text.toString(),
                    date = et_date.text.toString(),
                    time = et_time.text.toString(),
                    taskStatus
                )
            )
        } else {
            viewModel.insert(
                Task(
                    id = 0,
                    name = et_title.text.toString(),
                    date = et_date.text.toString(),
                    time = et_time.text.toString(),
                    "progress"
                )
            )
        }
    }

    private fun openTimePicker() {
        val timePicker = MaterialTimePicker.Builder()
            .setTimeFormat(TimeFormat.CLOCK_24H)
            .setTitleText("Select time")
            .build()

        timePicker.addOnPositiveButtonClickListener {
            val time = String.format(
                "%02d:%02d",
                timePicker.hour,
                timePicker.minute
            )
            et_time.setText(time)
        }
        timePicker.addOnNegativeButtonClickListener {
            timePicker.dismiss()
        }

        timePicker.show(childFragmentManager, "timePicker")
    }

    private fun openDatePicker() {
        val datePicker =
            MaterialDatePicker.Builder.datePicker()
                .setTitleText("Select date")
                .setSelection(MaterialDatePicker.todayInUtcMilliseconds())
                .build()
        datePicker.show(childFragmentManager, "datePicker")

        datePicker.addOnPositiveButtonClickListener {
            calendar.timeInMillis = it
            et_date.setText(df.format(calendar.time))
        }

        datePicker.addOnNegativeButtonClickListener {
            datePicker.dismiss()
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.setOnShowListener {
            val bottomSheetDialog = it as BottomSheetDialog
            val parentLayout = bottomSheetDialog.findViewById<View>(
                com.google.android.material.R.id.design_bottom_sheet
            )
            parentLayout?.let { bottomSheet ->
                val behaviour = BottomSheetBehavior.from(bottomSheet)
                val layoutParams = bottomSheet.layoutParams
                layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT
                bottomSheet.layoutParams = layoutParams
                behaviour.state = BottomSheetBehavior.STATE_EXPANDED
            }
        }
        return dialog
    }

    override fun onStop() {
        super.onStop()
        val imm =
            requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(view, 0)
    }
}