package com.test.room.taskslist.ui.doneTask

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.test.room.taskslist.R
import com.test.room.taskslist.TaskApplication
import com.test.room.taskslist.data.Task
import com.test.room.taskslist.ui.TaskViewModel
import com.test.room.taskslist.ui.TaskViewModelFactory
import com.test.room.taskslist.utils.MarginItemDecoration
import com.test.room.taskslist.utils.showDeleteTaskDialog
import kotlinx.android.synthetic.main.fragment_done_tasks.*

class DoneTaskFragment : Fragment(), DoneTaskAdapter.OnItemChangeListener {
    private val viewModel: TaskViewModel by activityViewModels {
        TaskViewModelFactory((requireActivity().application as TaskApplication).repository)
    }
    private lateinit var adapter: DoneTaskAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adapter = DoneTaskAdapter(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_done_tasks, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val layoutManager =
            StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL)
        rv_list.layoutManager = layoutManager
        rv_list.addItemDecoration(MarginItemDecoration(resources.getDimension(R.dimen.margin_16).toInt()))
        rv_list.adapter = adapter

        viewModel.getAllTasksStatus("done")
        viewModel.allTasksStatus.observe(viewLifecycleOwner, Observer { tasks ->
            tasks?.let {
                if (tasks.isEmpty()) {
                    rv_list.visibility = View.GONE
                    empty.visibility = View.VISIBLE
                } else {
                    rv_list.visibility = View.VISIBLE
                    empty.visibility = View.GONE
                    adapter.updateList(tasks)
                }
            }
        })
    }

    override fun update(task: Task) {
        val bundle = Bundle()
        bundle.putInt("taskId", task.id)
        bundle.putString("taskName", task.name)
        bundle.putString("taskDate", task.date)
        bundle.putString("taskTime", task.time)
        bundle.putString("taskStatus", task.status)
        findNavController().navigate(R.id.navigation_create_task, bundle)
    }

    override fun delete(task: Task) {
        showDeleteTaskDialog(getString(R.string.delete_message), this, viewModel, task)
    }
}